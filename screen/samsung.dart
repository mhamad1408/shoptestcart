import 'package:flutter/material.dart';
import 'package:scartp/screen/moblist.dart';
class Samsung extends StatefulWidget {
  @override
  _SamsungState createState() => _SamsungState();
}

class _SamsungState extends State<Samsung> {
  var mobilelist = [
    {
    'name': 'S20 Ultra',
    'Camera': '5 mgb',
    'Cpu': 'Snapdragon',
    'Price': '200'
  },
    {
    'name': 'P30 Pro',
    'Camera': '15 mgb',
    'Cpu': 'Snapdragon',
    'Price': '300'
  },
      {
    'name': 'Note 9',
    'Camera': '15 mgb',
    'Cpu': 'Snapdragon',
    'Price': '400'
  },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Samsung'),
        centerTitle: true,
        ),
        body: ListView.builder(
          itemCount: mobilelist.length,
          itemBuilder: (context, i){
            return  MobList(name:mobilelist[i]['name'], camera:mobilelist[i]['Camera'], cpu:mobilelist[i]['Cpu'] ,price:mobilelist[i]['Price'] ,);
          },
        ),
    );
  }
}

