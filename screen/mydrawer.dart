import 'package:flutter/material.dart';
import 'package:scartp/screen/categories.dart';
import 'package:scartp/screen/login.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
            child: Material(
            color: Color.fromRGBO(50, 75, 205, 1),
              child: ListView(
              children:<Widget>[
                  buildMenuItem(
                      text: 'TVs & Application',
                      icon: Icons.people,
                      onClicked: () => selectedItem(context, 0),
                    ),
                  ListTile(
                  title: Text("Bakety",style: TextStyle(color:Colors.black,fontSize: 15,)
                  ),
                  
                  trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                  onTap: (){},
                ),
                Divider(),
                 ListTile(
                  title: Text("Dairy & Eggs",style: TextStyle(color:Colors.black,fontSize: 15,)
                  ),
                  
                  trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                  onTap: (){},
                ),
                 ListTile(
                  title: Text("Cans & Jars",style: TextStyle(color:Colors.black,fontSize: 15,)
                  ),
                 
                  trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                  onTap: (){},
                ),
                 ListTile(
                  title: Text("Pantry",style: TextStyle(color:Colors.black,fontSize: 15,)
                  ),
                 
                  trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                  onTap: (){},
                ),
                  ListTile(
                  title: Text("Healthy Products",style: TextStyle(color:Colors.black,fontSize: 15,)
                  ),
                
                  trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                  onTap: (){},
                ),
                 ListTile(
                  title: Text("Breakfast Food",style: TextStyle(color:Colors.black,fontSize: 15,)
                  ),
                  
                  trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                  onTap: (){},
                ),
                 ListTile(
                  title: Text("Chocolate & Snacks",style: TextStyle(color:Colors.black,fontSize: 15,)
                  ),
              
                  trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                  onTap: (){},
                ),
                 ListTile(
                  title: Text("Frozen & Chiled",style: TextStyle(color:Colors.black,fontSize: 15,)
                  ),
        
                  trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                  onTap: (){},
                ),
                 ListTile(
                  title: Text("Beverages & Juices",style: TextStyle(color:Colors.black,fontSize: 15,)
                  ),
            
                  trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                  onTap: (){},
                ),
                   ListTile(
                  title: Text("Login",style: TextStyle(color:Colors.black,fontSize: 15,)
                  ),
            
                  trailing: Icon(Icons.arrow_right_alt_rounded,color: Colors.blue,size: 15,),
                  onTap: (){
                    Navigator.push(context,MaterialPageRoute(builder:(context)=>Login()));
                  },
                ),
              ],
          ),
            ),
   );
  }
}

  Widget buildMenuItem({
    @required String text,
    @required IconData icon,
    VoidCallback onClicked,
  }) {
    final color = Colors.white;
    final hoverColor = Colors.white70;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(text, style: TextStyle(color: color)),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    Navigator.of(context).pop();

    switch (index) {
      case 0:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => Categories(),
        ));
        break;
      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => Categories(),
        ));
        break;
    }
  }
