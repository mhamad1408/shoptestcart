// ignore: import_of_legacy_library_into_null_safe
//import 'package:carousel_pro/carousel_pro.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:scartp/screen/arrivalls.dart';
import 'package:scartp/screen/brands.dart';
import 'package:scartp/screen/mydrawer.dart';
import 'package:scartp/screen/offers.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var country;
   getPref() async{
  SharedPreferences preferences = await SharedPreferences.getInstance();
   country = preferences.getString('country');
   print(country);
  }
   @override
   void initState(){
     getPref();
     super.initState();
   }
  MyArticles (String imageVal,String heading,String subHeading,){
    return Container (
     width:150,
     child:new Row(
     mainAxisAlignment: MainAxisAlignment.spaceBetween,
     children:<Widget> [
      Expanded(
      child: Wrap(
       children:<Widget>[
         Image.asset(imageVal,height: 100,width: 200,fit: BoxFit.fitWidth),
         Directionality(
         textDirection: TextDirection.ltr,
         child:ListTile(
           leading:Wrap(
          children: <Widget>[
           Container(
            child:Text(heading,textAlign:TextAlign.center,style: TextStyle(fontSize:16,fontWeight: FontWeight.w600),), 
           ),
           Container(
            child:Text(subHeading,textAlign:TextAlign.center,style: TextStyle(color:Colors.blue),),
           ),
          ],
           ),
         ) ,
         ),
       ],
       ),
      ),
     ],
     ),
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Marche',
      home: Scaffold(
        appBar:AppBar(
          title: Text('Marche'),
          centerTitle: true,
          elevation: 6,
          actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: (){
           showSearch(context: context, delegate: SearchData());
          }),
          ],
          ) ,
          drawer:MyDrawer() ,
        body: ListView(
        scrollDirection: Axis.vertical,
        children:<Widget>[
         Container(
            height: 150.0,
            width: double.infinity,
            child: Carousel(
              images: [
               Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/w2.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/w3.jpg', fit: BoxFit.cover,),
            ],

            dotSpacing: 20,
            dotColor: Colors.white,
            dotBgColor: Colors.blue.withOpacity(0.5),
            overlayShadow: true,
            overlayShadowColors: Colors.blue,
            ),
         ),
        //   // End Carsouel
              Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
      Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            padding:EdgeInsets.all(10),
            height:50,
      child:Text('Brands',textAlign: TextAlign.left,style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800)
      ),   
     ),
   Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        padding:EdgeInsets.all(10),
        height:50,
       child:InkWell(
                child: Text('Show More',textAlign: TextAlign.right,//style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800),
         ),
           onTap: (){
              Navigator.push(context,MaterialPageRoute(builder:(context)=>Brands()));
             },
       ),
     ),
    ],
     ),
          Stack(
            children:<Widget>[
              Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:200,
              child:ListTile(
              title: Image.asset('assets/images/w1.jpg',fit:BoxFit.cover,height: 100,width: 200,),
              subtitle: Text('Smart Watch'),
              ),
          ),
          onTap: (){},
            ),
            InkWell(
                    child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:200,
              child: ListTile(
              title: Image.asset('assets/images/w1.jpg',fit:BoxFit.cover,height: 100,width: 200,),
              subtitle: Text('Smart Watch'),
              ),
          ),
              onTap: (){},
            ),
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:200,
              child: ListTile(
              title: Image.asset('assets/images/w1.jpg',fit:BoxFit.cover,height: 100,width: 200,),
              subtitle: Text('Smart Watch',textAlign: TextAlign.center,),
              ),
          ),
           onTap: (){},
            ),
      ],
      ),
       ),
     ),
       ],
    ),
     //New Arrivalls
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
         Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            padding:EdgeInsets.all(10),
            height:50,
      child:Text('New Arrivalls',textAlign: TextAlign.left,style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800)
      ),   
     ),
       Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        padding:EdgeInsets.all(10),
        height:50,
       child:InkWell(
        child: Text('Show More',textAlign: TextAlign.right,
         ),
        onTap: (){
              Navigator.push(context,MaterialPageRoute(builder:(context)=>Arrivalls()));
             },
       ),
     ),
    ],
     ),
          Stack(children:<Widget>[
                          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
            InkWell(
                          child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:200,
              child: MyArticles('assets/images/w6.jpeg',"TV-4K",""),
          ),
           onTap: (){},
            ),
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:200,
              child: MyArticles('assets/images/51tvxl9PlcL._SX380_BO1,204,203,200_.jpg',"Cread",""),
          ),
               onTap: (){},
            ),
            InkWell(
                          child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:200,
              child: MyArticles('assets/images/w2.jpg',"Watch",""),
          ),
          onTap: (){},
            ),
      ],
      ),
       ),
     ),

        ],
       ),
     //Special Offers
         Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
         Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            padding:EdgeInsets.all(10),
            height:50,
      child:Text('Special Offers',textAlign: TextAlign.left,style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800)
      ),   
     ),

       Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        padding:EdgeInsets.all(10),
        height:50,
       child:InkWell(
                child: Text('Show More',textAlign: TextAlign.right,//style: TextStyle(color:Colors.black54,fontSize: 20,fontWeight: FontWeight.w800),
         ),
          onTap: (){
              Navigator.push(context,MaterialPageRoute(builder:(context)=>Offers()));
       },
       ),
     ),
    ],
     ),
          Stack(children:<Widget>[
           Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
            InkWell(
                          child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:200,
              child: MyArticles('assets/images/w7.png'," ValHala",""),
          ),
          onTap: (){},
            ),
            InkWell(
                          child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:200,
              child: MyArticles('assets/images/w8.jpg',"Cread",""),
          ),
             onTap: (){},
            ),
            InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:200,
              child: MyArticles('assets/images/w9.jpg',"Ezio",""),
          ),
              onTap: (){},
            ),
      ],
      ),
       ),
     ),
        ],
       ),
        ],
        ),
      ),
    );
  }
}


class SearchData extends SearchDelegate<String>{
  @override
  List<Widget> buildActions(BuildContext context) {
      //  buildActions
      return [
        IconButton(icon: Icon(Icons.clear), onPressed: (){

        }),
      ];
    }
  
    @override
    Widget buildLeading(BuildContext context) {
      //  buildLeading
      return IconButton(
      icon: Icon(Icons.arrow_back), onPressed: (){

        }
      );
    }
  
    @override
    Widget buildResults(BuildContext context) {
      //  buildResults
      return null;
    }
  
    @override
    Widget buildSuggestions(BuildContext context) {
    // TODO: buildSuggestions
    return Text('Search');
    
  }

}