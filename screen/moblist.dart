import 'package:flutter/material.dart';
import 'package:scartp/screen/details.dart';
class MobList extends StatelessWidget {
  final name ;
  final camera ;
  final cpu ;
  final price ;

   MobList({ this.name, this.camera, this.cpu, this.price});
  @override
  Widget build(BuildContext context) {
    return InkWell(
           child: Container(
            height: 100,
            width: 100,
             child: Card(
              child:Row(
            children: <Widget>[
            Expanded(flex: 1,child: 
            Image.asset('assets/images/w17.jpg'),
            ),
            Expanded(flex: 2,
            child: Container(
            height: 100,
            alignment: Alignment.center,
            child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(name,style:TextStyle(fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                Row(
               mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                Expanded(
                child: Text('Camera:',),
                ),
                Expanded(
                child: Text(camera,style: TextStyle(color: Colors.grey),
                ),
                ),
                ],
                ),
                Row(
                  children: [
                    Expanded(
                    child: Text('CPU:'),
                    ),
                    Expanded(
                    child: Text(cpu,style: TextStyle(color: Colors.grey),
                    ),
                    ),
                  ],
                ),
                Container(
                margin: EdgeInsets.only(top: 5),
                child: Expanded(child: Text("price:$price",style:TextStyle(color: Colors.red),)),
                ),
                  SizedBox(
                    height: 3,
                    ),
                Text('More Details',style: TextStyle(color: Colors.blue),),
              ],
            )
              ),
              ),
           ],
           ),
           ),
           ),
           onTap: (){
              Navigator.push(context,MaterialPageRoute(builder:(context)=>Details()));
           },
         );
  }
}